import { createContext } from "react";
import { useAuthProvider } from "../hooks/useAuth";

export const AuthContext = createContext();

export function AuthProvider({ children }) {
  const auth = useAuthProvider();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}
