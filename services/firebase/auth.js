import {
  createUserWithEmailAndPassword,
  getAuth,
  GithubAuthProvider,
  GoogleAuthProvider,
  onAuthStateChanged,
  signInWithPopup,
  signInWithEmailAndPassword as _signInWithEmailAndPassword,
  signOut as _signOut,
} from "firebase/auth";
import firebase from "./";

const auth = getAuth(firebase);

const signInWithGitHub = () => {
  return signInWithPopup(auth, new GithubAuthProvider())
    .then((result) => {
      return { user: result?.user || null };
    })
    .catch((error) => {
      console.log(error);
      return {
        error: error?.code || "Issue signing into GitHub at the moment",
      };
    });
};

const signInWithGoogle = () => {
  return signInWithPopup(auth, new GoogleAuthProvider())
    .then((result) => {
      return { user: result?.user || null };
    })
    .catch((error) => {
      console.log(error);
      return {
        error: error?.code || "Issue signing into Google at the moment",
      };
    });
};

const signUpWithEmailAndPassword = (email, password) => {
  const { isValid: isEmailValid, error: emailError } = validateEmail(email);
  if (!isEmailValid) {
    return {
      error: emailError,
    };
  }

  const { isValid: isPasswordValid, error: passwordError } =
    validatePassword(password);
  if (!isPasswordValid) {
    return {
      error: passwordError,
    };
  }

  return createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      return { user: userCredential?.user || null };
    })
    .catch((error) => {
      console.log(error);
      return {
        error:
          replaceStringWithContent(formatErrorMessage(error?.code), {
            email,
          }) || "Issue creating a new account at the moment",
      };
    });
};
const signInWithEmailAndPassword = (email, password) => {
  const { isValid: isEmailValid, error: emailError } = validateEmail(email);
  if (!isEmailValid) {
    return {
      error: emailError,
    };
  }

  const { isValid: isPasswordValid, error: passwordError } =
    validatePassword(password);
  if (!isPasswordValid) {
    return {
      error: passwordError,
    };
  }

  return _signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      return { user: userCredential?.user || null };
    })
    .catch((error) => {
      console.log(error);
      return {
        error:
          replaceStringWithContent(formatErrorMessage(error?.code), {
            email,
          }) || "Issue signing in with email and password at the moment",
      };
    });
};

const signOut = () => {
  return _signOut(auth)
    .then(() => {
      return { isLoggedOut: true };
    })
    .catch((error) => {
      console.log(error);
      return {
        error: error?.code || "Issue logging out at the moment",
      };
    });
};

const replaceStringWithContent = (str = "", content = {}) => {
  const { email = "" } = content;
  if (!str || !str.length || !Object.keys(content).length) return "";
  return str.replace(/___EMAIL___/, email);
};

const formatErrorMessage = (errorMessage) => {
  if (!errorMessage || !errorMessage.length) return null;

  switch (errorMessage) {
    case "auth/user-not-found":
      return "No account associated with ___EMAIL___";

    default:
      return null;
  }
};

const validateEmail = (email) => {
  const emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (
    !email ||
    typeof email !== "string" ||
    !email.length ||
    !emailRegex.test(email)
  ) {
    return {
      isValid: false,
      error: "Invalid email address",
    };
  }

  return {
    isValid: true,
    error: null,
  };
};

const validatePassword = (password) => {
  const passwordRegex =
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/;

  if (
    !password ||
    typeof password !== "string" ||
    !password.length ||
    !passwordRegex.test(password)
  ) {
    return {
      isValid: false,
      error:
        "Invalid password (minimum 8 characters, at least 1 upper case English letter, 1 lower case English letter, 1 number and 1 special character",
    };
  }

  return {
    isValid: true,
    error: null,
  };
};

const firebaseAuthService = {
  auth,
  onAuthStateChanged,
  signInWithGitHub,
  signInWithGoogle,
  signUpWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
};

export default firebaseAuthService;
