import { initializeApp, getApps, getApp } from "firebase/app";
import config from "./config";

const firebase = !getApps().length ? initializeApp(config, "ff") : getApp("ff");

export default firebase;
