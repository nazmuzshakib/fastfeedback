import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../context/authContext";
import firebaseAuthService from "../services/firebase/auth";

export function useAuthProvider() {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const signInWithGitHub = async () => {
    setError(null);
    setLoading(true);
    const { user, error } = await firebaseAuthService.signInWithGitHub();
    setLoading(false);
    user ? setUser(user) : setError(error);
  };

  const signInWithGoogle = async () => {
    setError(null);
    setLoading(true);
    const { user, error } = await firebaseAuthService.signInWithGoogle();
    setLoading(false);
    user ? setUser(user) : setError(error);
  };

  const signUpWithEmailAndPassword = async (email, password) => {
    setError(null);
    setLoading(true);
    const { user, error } =
      await firebaseAuthService.signUpWithEmailAndPassword(email, password);
    setLoading(false);
    user ? setUser(user) : setError(error);
  };

  const signInWithEmailAndPassword = async (email, password) => {
    setError(null);
    setLoading(true);
    const { user, error } =
      await firebaseAuthService.signInWithEmailAndPassword(email, password);
    setLoading(false);
    user ? setUser(user) : setError(error);
  };

  const signOut = async () => {
    await firebaseAuthService.signOut();
  };

  useEffect(() => {
    const unsubscribe = firebaseAuthService.onAuthStateChanged(
      firebaseAuthService.auth,
      (user) => {
        if (user) {
          setUser(user);
        } else {
          setUser(null);
        }
        return () => unsubscribe();
      }
    );
  }, []);

  return {
    user,
    loading,
    error,
    signInWithGitHub,
    signInWithGoogle,
    signUpWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOut,
  };
}

export const useAuth = () => useContext(AuthContext);
